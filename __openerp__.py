# -*- coding: utf-8 -*-
{
    'name': "جمعية المحافظة علي القرآن الكريم",

    'summary': """
        إدارة جمعية المحافظة علي القرآن الكريم وما يلتحق بها من أنشطة مثل
        حضانة أنا المسلم , حضانة الحافظ ,  دار البيان لتحفيظ القرآن""",


    'author': "Smart Solutions",
    'website': "http://www.smart.com",

    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'web_rtl', 'report_rtl'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        
        'data/actions.xml',
        'data/menus.xml',

        'views/members.xml',
        'views/staff.xml',
        'views/kindergarten.xml',
        'views/mothers.xml',
        'views/children.xml',
        'views/accounting.xml',
        'views/medical.xml',

        'reports/header_template_report.xml',
        'reports/medical_card_report.xml',
    ],
    'application': True,
}
