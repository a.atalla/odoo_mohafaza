# -*- coding: utf-8 -*-
from openerp import models, fields, api
import logging


_logger = logging.getLogger(__name__)

class Members(models.Model):

    _name = 'mohafaza.members'
    name = fields.Char('إسم العضو', required=True)
    membership_num = fields.Char('رقم العضوية', required=True)
    membership_date = fields.Date('تاريخ العضوية', required=True)
    national_num = fields.Char('الرقم القومي')
    job = fields.Char('الوظيفة')
    birth_date = fields.Date('تاريخ الميلاد')
    education_degree = fields.Char('المؤهل الدراسي')
    address = fields.Char('العنوان')
    cell_phone = fields.Char('رقم الهاتف')
    email = fields.Char('البريد الإلكتروني')

    memberships_ids = fields.One2many('mohafaza.account.memberships', 'member_id', string='رسوم العضوية')

    '''
    @api.model
    def create(self, member):
        account_obj = self.env['mohafaza.account.chart']
        account_obj.create({'name': member['name']})
        new_member = super(Members, self).create(member)
        return new_member
    '''
