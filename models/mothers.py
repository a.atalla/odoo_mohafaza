# -*- coding: utf-8 -*-
from openerp import models, fields, api


class Mother(models.Model):
    _name = 'mohafaza.mothers'
    name = fields.Char(string='إسم اﻷم', required=True)
    national_num = fields.Char(string='الرقم القومي')
    education_degree = fields.Char(string='المؤهل الدراسي')
    job = fields.Char(string='الوظيفة')
    address = fields.Char(string='العنوان')
    cell_phone = fields.Char(string='رقم الهاتف')
    email = fields.Char(string='البريد الإلكتروني')
