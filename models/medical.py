# -*- coding: utf-8 -*-
from openerp import models, fields


class Doctor(models.Model):
    _name = 'mohafaza.medical.doctors'

    name = fields.Char(string='إسم الطبيب')
    cell_phone = fields.Char(string='رقم الهاتف المحمول')


class MedicalCard(models.Model):
    _name = 'mohafaza.medical.cards'

    child_id = fields.Many2one('mohafaza.children', string='إسم الطفل', required=True)
    doctor_id = fields.Many2one('mohafaza.medical.doctors', string='إسم الطبيب', required=True)
    date = fields.Date(string='تاريخ الكشف', default=fields.Date().today(), required=True)
    state = fields.Text(string='الحالة الصحية للطفل')
    recommendation = fields.Text(string='توصيات الطبيب')
