# -*- coding: utf-8 -*-
from openerp import models, fields


class Level(models.Model):
    _name = 'mohafaza.kindergarten.levels'
    name = fields.Char(string='المستوي', required=True)


class ClassRoom(models.Model):
    _name = 'mohafaza.kindergarten.classrooms'
    name = fields.Char(string='إسم الفصل', required=True)
    level_id = fields.Many2one('mohafaza.kindergarten.levels', string='المستوي', required=True, ondelete='restrict')
    staff_id = fields.Many2one('mohafaza.staff', string='مشرفة الفصل', required=True, ondelete='restrict')
    children_ids = fields.One2many('mohafaza.children', 'class_room', string='اﻷطفال')
