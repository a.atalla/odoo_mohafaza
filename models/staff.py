# -*- coding: utf-8 -*-
from openerp import fields, models, api
import datetime
import logging

_logger = logging.getLogger(__name__)


class Job(models.Model):

    _name = 'mohafaza.jobs'
    name = fields.Char('الوظيفة', required=True)


class Staff(models.Model):

    _name = 'mohafaza.staff'
    name = fields.Char('الإسم', required=True)
    national_num = fields.Char('الرقم القومي')
    address = fields.Char('العنوان')
    cell_phone = fields.Char('رقم الهاتف')
    email = fields.Char('البريد الإلكتروني')
    job = fields.Many2one('mohafaza.jobs', string='الوظيفة', ondelete='restrict')

    '''
    @api.model
    def create(self, staff):
        account_obj = self.env['mohafaza.account.chart']
        account_obj.create({'name': staff['name']})
        new_staff = super(Staff, self).create(staff)
        return new_staff
    '''

class StaffAttendance(models.Model):

    _name = 'mohafaza.staff.attend'
    staff_id = fields.Many2one('mohafaza.staff', string='الموظف', required=True, ondelete='restrict')
    day = fields.Date('اليوم', required=True, default=fields.Date.today())
    is_attend = fields.Selection(string='الحالة',
                                 selection=[('absent', 'غائب'), ('attendant', 'حاضر')])
    notes = fields.Text(string='ملاحظات')

    _sql_constraints = [
        ('unique_staff_attendance', 'unique(staff_id, day)', 'تم التسجيل من قبل')]
