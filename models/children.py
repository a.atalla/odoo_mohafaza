# -*- coding: utf-8 -*-
from openerp import models, fields, api
import datetime
import logging

_logger = logging.getLogger(__name__)


class ChildTaker(models.Model):
    _name = 'mohafaza.children.takers'
    name = fields.Char(string='الإسم', required=True)
    relative = fields.Char(string='صلة القرابة')
    cell_phone = fields.Char(string='رقم الهاتف')
    child_id = fields.Many2one('mohafaza.children', string='إسم الطفل', ondelete='restrict')


class Child(models.Model):
    _name = 'mohafaza.children'

    # personal Information
    name = fields.Char(string='إسم الطفل', required=True)
    birth_date = fields.Date(string='تاريخ الميلاد', required=True)
    class_room = fields.Many2one('mohafaza.kindergarten.classrooms', string='الفصل', required=True, ondelete='restrict')
    guardian_id = fields.Many2one('mohafaza.members', string='ولي أمر الطفل', required=True, ondelete='restrict')
    guardian_relative = fields.Char(string='صلة القرابة')
    mother_id = fields.Many2one('mohafaza.mothers', string='اﻷم', ondelete='restrict')
    sibling_num = fields.Integer(string='عدد اﻷشقاء')
    taker_ids = fields.One2many('mohafaza.children.takers', 'child_id', string='اﻷشخاص اللذين يحق لهم اصطحاب الطفل')
    child_order = fields.Integer(string='ترتيب الطفل بين أشقائه')
    fav_color = fields.Char(string='لون الطفل المفضل')
    fav_name = fields.Char(string='الإسم المفضل لدي الطفل')
    fav_game = fields.Char(string='لعبة الطفل المفضلة')
    hobby = fields.Char(string='هواية الطفل المفضلة')
    strangers = fields.Selection([('social', 'إجتماعي'), ('deny', 'يستنكر'), ('fear', 'خوف شديد')],
                                    string='علاقة الطفل بالأغراب')
    other_children = fields.Selection([('play', 'يلعب معهم'), ('deny', 'يستنكر'), ('fear', 'ينفر منهم')],
                                        string='علاقة الطفل بالأطفال الآخرين')
    good_manners = fields.Text(string='صفات حسنة في طفلك')
    bad_manners = fields.Text(string='صفات سيئة في طفلك')

    # Medecal Information
    diseases = fields.Text(string='هل يعاني الطفل من أي أمراض مزمنة؟')
    vulnerability = fields.Text(string='هل يعاني الطفل من أي حساسية؟')
    drugs = fields.Text(string='هل يأخذ الطفل أي أدوية بصفة مستمرة؟')
    emergency = fields.Selection([('family', 'الإتصال بالأهل'),('doctor', 'الأتصال بطبيب الدار')],
                                    string='التصرف المفضل في حالة تعرض الطفل لظرف صحي طارئ')

    # kindergarten
    why_us = fields.Text(string='لماذا اخترت حضانة أنا المسلم؟')
    how_find = fields.Text(string='كيف تعرفت علي الحضانة')
    expectation = fields.Text(string='ماذا تتوقع أن يستفيد طفلك خلال وجوده بالحضانة؟ ')

    # Accounting
    kindergarten_fees_ids = fields.One2many('mohafaza.account.kindergarten.fees', 'child_id', 'الحضانة')
    bus_fees_ids = fields.One2many('mohafaza.account.bus.fees', 'child_id', 'الباص')


    '''
    @api.model
    def create(self, child):
        account_obj = self.env['mohafaza.account.chart']
        account_obj.create({'name': child['name']})
        new_child = super(Child, self).create(child)
        return new_child
    '''

class ChildrenAttendance(models.Model):

    _name = 'mohafaza.children.attend'
    child_id = fields.Many2one('mohafaza.children', 'الطفل', required=True, ondelete='restrict')
    day = fields.Date('اليوم', required=True, default=fields.Date.today())
    is_attend = fields.Selection(string='الحالة',
                                 selection=[('absent', 'غائب'), ('attendant', 'حاضر')])
    notes = fields.Text(string='ملاحظات')

    _sql_constraints = [
        ('unique_children_attendance', 'unique(child_id, day)', 'تم التسجيل من قبل')]
